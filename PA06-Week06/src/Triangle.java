import java.util.Scanner;

public class Triangle {
	
	public static void main(String[] args ) {
		
		//Initialize Variables
		Scanner input = new Scanner(System.in);
		int number;
		
		//Console Output
		System.out.println("| This program will display square and triangle with the number entered.");
		System.out.println();
		System.out.print("Please enter a number between 5-15: ");
		number = input.nextInt();
		
		//Calculation
		do {
			System.out.println("You've entered a number other than 5 through 15. Please try again. || There's a chance you did enter a number between 5-15, so try again.");
			System.out.println();
			System.out.print("Please enter a number between 5-15: ");
			number = input.nextInt();
		}while(number < 5 || number > 15 );
		//I'm most confused about this and why it doesn't work. Do I need to put the entire calculation in the do while having and else if? 
		
		//Calculation -- Square
		System.out.println("Printing Square:");
		for(int i = 1; i <= number; i++) {
			for(int j = 1; j <= number; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
		
		System.out.println();
		
		//Calculation -- Triangle
		System.out.println("Printing Triangle: ");
		for(int i = 1; i <= number; i++) {
			for(int j = i; j < number; j++) {
				System.out.print(" " + " ");
			}
			for(int k = i; k > 1; k--) {
				System.out.print(k + " ");
			}
			for(int k = 1; k <= i; k++) {
				System.out.print(k + " ");
			}
			System.out.println();
		}
	}
}
