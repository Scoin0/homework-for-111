import java.util.Scanner;

public class Phrase {
	
	public static void main(String [] args) {
		
		//Initialize Variables
		Scanner phrase = new Scanner(System.in);
		int count;
		int vowelsA = 0, vowelsE = 0, vowelsI = 0, vowelsO = 0, vowelsU = 0;
		String word;
		 
		//Console Output
		System.out.println("| This program will display the characters in a phrase and display how many Vowels are in that phrase.");
		System.out.println();
		System.out.println("Please enter a phrase: ");
		word = phrase.nextLine();
		
		//Calculation.
		for(count = 0; count < word.length(); count++) {
			System.out.println(count + "| " + word.charAt(count));
		}
		
		System.out.println();
		System.out.println("This phrase also contains: ");
		
		for(int i = 0; i < word.length(); i++) {
			char v = word.charAt(i);
			if(v == 'a' || v == 'A') {
				vowelsA++;
			}
			if(v == 'e' || v == 'E') {
				vowelsE++;
			}
			if(v == 'i' || v == 'I') {
				vowelsI++;
			}
			if(v == 'o' || v == 'O') {
				vowelsO++;
			}
			if(v == 'u' || v == 'U') {
				vowelsU++;
			}
		}
		System.out.println(vowelsA + " A's");
		System.out.println(vowelsE + " E's");
		System.out.println(vowelsI + " I's");
		System.out.println(vowelsO + " O's");
		System.out.println(vowelsU + " U's");
	}
}
