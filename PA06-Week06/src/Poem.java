import java.util.Scanner;

public class Poem {
	
	public static void main(String[] args) {
		
		//Initialize variables.
		Scanner input = new Scanner(System.in);
		int number;
		
		//Console Output.
		System.out.println("| This program will display a poem depending on the number typed.");
		System.out.println();
		System.out.println("Please enter a number between 1-7 (Press 0 to exit)");
		number = input.nextInt();
		
		//Figuring out what number was called.
		while(number < 8) {
			if(number == 1) {
				System.out.println("Monday's child is fair of face");
			}
			
			if(number == 2) {
				System.out.println("Tuesday's child is full of grace");
			}
			
			if(number == 3) {
				System.out.println("Wednesday's child is full of woe");
			}
			
			if(number == 4) {
				System.out.println("Thursday's child has far to go");
			}
			
			if(number == 5) {
				System.out.println("Friday's child is loving and giving");
			}
			
			if(number == 6) {
				System.out.println("Saturday's child works hard for its living");
			}
			
			if(number == 7) {
				System.out.println("But the child that is born on the Sabbath day is bonny and blithe, and good and gay");
			}
			
			if(number == 0) {
				break;
			}
			number = input.nextInt();
		}
		
		while(number >= 7) {
			System.out.println("You entered a number that is not between 1 and 7.");
			number = input.nextInt();
		}
	}
}
