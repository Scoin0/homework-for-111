import java.util.Date;

/**
 * The Book Interface
 * 
 * @author John Lewis
 *
 */

public interface IBook {
	
	/**
	 * Sets the Books Reference ID
	 * 
	 * @param ID 		Reference ID
	 */
	void setID(int ID);
	/**
	 * Returns the Books Reference ID
	 * 
	 * @return ID 		Reference ID
	 */
	int getID();
	
	/**
	 * Sets the Books Title
	 * 
	 * @param title		Book Title
	 */
	void setTitle(String title);
	/**
	 * Gets the Books Title
	 * 
	 * @return title	Book Title
	 */
	String getTitle();
	
	/**
	 * Sets the Books Author
	 * 
	 * @param author	Book Author
	 */
	void setAuthor(String author);
	/**
	 * Returns the Books Author
	 * 
	 * @return author	Book Author
	 */
	String getAuthor();
	
	/**
	 * Sets the Books availability status
	 * 
	 * @param checked	Books Availability 
	 */
	void setChecked(boolean checked);
	/**
	 * Returns if the book is available or not
	 * 
	 * @return checked 	Books Availability
	 */
	boolean isChecked();
	
	//TODO
	void patron(int patron);
	int patronChecked();
	
	/**
	 * Returns the Due date for the book
	 * 
	 * @return date		Books Due Date
	 */
	Date getDue();
	
	/**
	 * Sets the Due Date for the book
	 * 
	 * @param date		Books Due Date
	 */
	void setDue(int date);
}
