import java.util.Calendar;
import java.util.Date;

/**
 * Book Object Class
 * 
 * @author John Lewis
 *
 */

public class Book implements IBook {
	
	//Variables
	private int ID;
	private String title;
	private String author;
	private boolean checked;
	private int whoChecked;
	private Calendar date;
	
	public Book() {
		ID = 0;
		title = null;
		author = null;
		checked = false;
		whoChecked = 0;
	}
	
	/**
	 * Book Object
	 * 
	 * @param ID			Book Reference ID
	 * @param title			Book Title
	 * @param author		Book Author
	 * @param checked		Book isCheckedOut
	 * @param whoChecked	
	 * @param date			Book dueDate
	 */
	public Book(int ID, String title, String author, Boolean checked, int whoChecked, Calendar date) {
		this.ID = ID;
		this.title = title;
		this.author = author;
		this.checked = checked;
		this.whoChecked = whoChecked;
		this.date = date;
	}
	
	/**
	 * Sets the Books Reference ID
	 * 
	 * @param ID 	Book Reference ID
	 */
	@Override
	public void setID(int ID) {
		this.ID = ID;
	}
	
	/**
	 * Returns the books Reference ID
	 * 
	 * @return ID	Book Reference ID
	 */
	public int getID() {
		return ID;
	}

	/**
	 * Sets the Books Title
	 * 
	 * @param title		Book Title
	 */
	@Override
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the Books Title
	 * 
	 * @return title	Book Title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the Books Author
	 * 
	 * @param author	Book Author
	 */
	@Override
	public void setAuthor(String author) {
		this.author = author;
	}
	
	/**
	 * Returns the Books Author
	 * 
	 * @return author	Book Author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Sets the Books availability status
	 * 
	 * @param checked	Books Availability 
	 */
	@Override
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	/**
	 * Returns if the book is available or not
	 * 
	 * @return checked 	Books Availability
	 */
	public boolean isChecked() {
		return checked;
	}
	//TODO
	@Override
	public void patron(int patron) {
		this.whoChecked = patron;
	}
	
	public int patronChecked() {
		return whoChecked;
	}

	/**
	 * Sets the Due date for the book
	 */
	public void setDue(int date) {
		Calendar date1 = Calendar.getInstance();
		date1 = (Calendar) date1.clone();
		date1.add(Calendar.DATE, 7);
		this.date = date1;
	}
	
	/**
	 * Returns the Due Date for the Book
	 */
	@Override
	public Date getDue() {
		return date.getTime();
	}
	
	/**
	 * To String Method
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		if(checked == false) {
			sb.append("Book ID: " + getID() + "\n")
			.append("Book Title: " + getTitle() + "\n")
			.append("Book Author: " + getAuthor() + "\n");
		} else {
			sb.append("Book ID: " + getID() + "\n")
			.append("Book Title: " + getTitle() + "\n")
			.append("Book Author: " + getAuthor() + "\n")
			.append("Patron who checked out this book: " + Library.patrons.get(patronChecked()).getFirstName() + " " + Library.patrons.get(patronChecked()).getLastName() + " \n")
			.append("Due By: " + getDue() + "\n");
		}
		return sb.toString();
	}
}
