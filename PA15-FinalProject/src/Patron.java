
/**
 * Patron Object Class
 * 
 * @author John Lewis
 *
 */

public class Patron {
	
	//Variables
	private int ID;
	private String firstName;
	private String lastName;
	private int check;
	private int whoTookOut;
	
	private boolean hasCheckedOut;
	
	public Patron() {
		ID = 0;
		firstName = null;
		lastName = null;
		check = 0;
		whoTookOut = 0;
	}
	
	/**
	 * Patron Object
	 * 
	 * @param ID			Patron Reference ID
	 * @param firstName		Patron First Name
	 * @param lastName		Patron Last Name
	 * @param check			Patron Checkout Status
	 * @param whoTookOut	
	 * @param hasCheckedOut	Patron hasCheckedOut a Book
	 */
	public Patron(int ID, String firstName, String lastName, int check, int whoTookOut, boolean hasCheckedOut) {
		this.ID = ID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.check = check;
		this.whoTookOut = whoTookOut;
		this.hasCheckedOut = hasCheckedOut;
	}

	/**
	 * Returns the Patron Reference ID
	 * 
	 * @return ID	Patron Reference ID
	 */
	public int getID() {
		return ID;
	}

	/**
	 * Sets the Patron Reference ID
	 * 
	 * @param ID	Patron Reference ID
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * Returns the Patron First Name
	 * 
	 * @return firstName	Patron First Name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the Patron First Name
	 * 
	 * @param firstName 	Patron First Name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Returns the Patron Last Name
	 * 
	 * @return lastName		Patron Last Name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the Patron Last Name
	 * 
	 * @param lastName		Patron Last Name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the amount of the books the Patron can take out
	 * 
	 * @return check		Patron Checkout Amount
	 */
	public int getCheck() {
		return check;
	}

	/**
	 * Sets the amount of books the Patron can take out
	 * 
	 * @param check			Patron Checkout Amount
	 */
	public void setCheck(int check) {
		this.check = check;
	}
	
	/**
	 * Return the Patron checking out a book. This is to obtain a list of people who checked out a book later.
	 * 
	 * @return hasCheckedOut	Check if Patron has checked out a book
	 */
	public boolean getCheckedOut() {
		return hasCheckedOut;
	}
	
	/**
	 * Sets the Patron checking out a book. This is to obtain a list of people who checked out a book later.
	 * 
	 * @param hasCheckedOut 	Patron hasCheckOut
	 */
	public void hasCheckedOut(boolean hasCheckedOut) {
		this.hasCheckedOut = hasCheckedOut;
	}
	
	//TODO
	/**
	 * @return the whoTookOut
	 */
	public int getWhoTookOut() {
		return whoTookOut;
	}

	/**
	 * @param whoTookOut the whoTookOut to set
	 */
	public void setWhoTookOut(int whoTookOut) {
		this.whoTookOut = whoTookOut;
	}
	

	/**
	 * To String Method
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();
		if(check == 0) {
			s.append("Patron ID: #" + getID() + "\n")
			.append("First Name: " + getFirstName() + "\n")
			.append("Last Name: " + getLastName() + "\n")
			.append("This Patron cannot check out any more books.\n");
		} else {
			s.append("Patron ID: #" + getID() + "\n")
			.append("First Name: " + getFirstName() + "\n")
			.append("Last Name: " + getLastName() + "\n")
			.append("Remaining Checkout Size: " + getCheck() + "\n");
		}
		return s.toString();
	}
}
