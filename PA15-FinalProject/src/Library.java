import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

/**
 * Main Library Class
 * 
 * Creates books, patrons, edits both of them and the ability to check in and check out books.
 * 
 * @author John Lewis
 *
 */

public class Library {
	
	//Creates Lists for both Books and Patrons
	static List<Book> books = new ArrayList<Book>();
	static List<Patron> patrons = new ArrayList<Patron>();
	//Store the amount of Books and Patrons added
	private static int PID = 0;
	private static int BID = 0;
	
	/**
	 * Main Method
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Welcome to the Library System!");
		displayMenu();
		System.out.print("Option: ");
		int option = input.nextInt();
		do {
			switch(option) {
			case 1:
				addPatron();
				break;
			case 2:
				addBook();
				break;
			case 3:
				editPatron();
				break;
			case 4:
				editBook();
				break;
			case 5:
				System.out.println("Here's our Patrons: ");
				displayPatron();
				break;
			case 6:
				System.out.println("Here's our list of Books: ");
				displayBook();
				break;
			case 7:
				checkoutBook();
				break;
			case 8:
				checkinBook();
				break;
			default:
				System.out.println("Invalid Selection");
				break;
			}
			input = new Scanner(System.in);
			displayMenu();
			System.out.print("Option: ");
			option = input.nextInt();
		}while(option != 9);
	}
	
	/**
	 * Checkout a Book
	 */
	public static void checkoutBook() {
		Calendar date;
		Scanner input = new Scanner(System.in);
		System.out.println("Please select the book you'd like to checkout: ");
		displayBook();
		System.out.print("Book ID: ");
		int bookID = input.nextInt();
		System.out.println();
		System.out.println("Please select the patron who'd like to check the book out: ");
		displayPatron();
		System.out.print("Patron ID: ");
		int patronID = input.nextInt();
		System.out.println();
		
		if(books.get(bookID).isChecked() == true) {
			System.out.println("This book has already been checked out.");
		} else {
			if(patrons.get(patronID).getCheck() != 0) {
				System.out.println(patrons.get(patronID).getFirstName() + " has checked out " + books.get(bookID).getTitle() + " by " 
						+ books.get(bookID).getAuthor());
				books.get(bookID).setDue(7);
				System.out.println("The book is due on: " + books.get(bookID).getDue());
				books.get(bookID).setChecked(true);
				books.get(bookID).patron(patronID);
				patrons.get(patronID).setCheck(patrons.get(patronID).getCheck()-1);
				patrons.get(patronID).setWhoTookOut(bookID);
				System.out.println("This patron can now borrow " + patrons.get(patronID).getCheck() + " more books.");
			} else {
				System.out.println("This patron cannot borrow any more books.");
			}
		}
	}
	
	/**
	 * Check-in a Book
	 */
	public static void checkinBook() {
		Scanner input = new Scanner(System.in);
		System.out.println("Please select the book you'd like to check in: ");
		for(Book b : books) {
			if(b.isChecked() == true) {
				System.out.println(b.toString());
			} else if(books.isEmpty()){ 
				System.out.println("There are no books in the system.");
			}
		}
		System.out.print("Book ID: ");
		int bookID = input.nextInt();
		System.out.println();
		System.out.println("Please select the patron who'd like to return the book: ");
		for(Patron p : patrons) {
			if(p.getCheckedOut() == true) {
				System.out.println(p.toString());
			} else if(patrons.isEmpty()) {
				System.out.println("There are no patrons in the system.");
			}
		}
		System.out.print("Patron ID: ");
		int patronID = input.nextInt();
		
		books.get(bookID).setChecked(false);
		books.get(bookID).patron(0);
		patrons.get(patronID).setCheck(patrons.get(patronID).getCheck() + 1);
		patrons.get(patronID).setWhoTookOut(0);
		patrons.get(patronID).hasCheckedOut(false);
		System.out.println(books.get(bookID).getTitle() + " by " + books.get(bookID).getAuthor() + " has been returned.");
		System.out.println("The Patron can borrow " + patrons.get(patronID).getCheck());
	}
	
	/**
	 * Add Book
	 */
	public static void addBook() {
		Book book = new Book();
		System.out.print("Enter the Title of the Book: ");
		Scanner input = new Scanner(System.in);
		String title = input.nextLine();
		book.setTitle(title);
		System.out.print("Enter the Author of the Book: ");
		String author = input.nextLine();
		book.setAuthor(author);
		book.setID(BID++);
		book.setChecked(false);
		books.add(book);
	}
	
	/**
	 * Display Book
	 */
	public static void displayBook() {
		for(Book b : books) {
			System.out.println(b.toString());
		} if(books.isEmpty()) {
			System.out.println("There are no books in the system. ");
		}
	}
	
	/**
	 * Edit Book
	 */
	public static void editBook() {
		Scanner input = new Scanner(System.in);
		System.out.println("Select the book you'd like to edit");
		displayBook();
		System.out.print("Book ID: ");
		int book = input.nextInt();
		if(books.get(book) != null) {
			System.out.println("Editing Book ID: #" + books.get(book).getID());
			System.out.print("Enter the title of the book: ");
			String title = input.next();
			books.get(book).setTitle(title);
			System.out.print("Enter the author of the book: ");
			String author = input.next();
			books.get(book).setAuthor(author);
			System.out.println("Book updated.");
			System.out.println(books.get(book).toString());
		} else {
			System.out.println("There are no books to edit.");
		}
	}
	
	/**
	 * Add Patron
	 */
	public static void addPatron() {
		Patron patron = new Patron();
		System.out.print("Enter the First Name of the Patron: ");
		Scanner input = new Scanner(System.in);
		String firstName = input.nextLine();
		patron.setFirstName(firstName);
		System.out.print("Enter the Last Name of the Patron: ");
		String lastName = input.nextLine();
		patron.setLastName(lastName);
		patron.setCheck(3);
		patron.setID(PID++);
		patrons.add(patron);
	}
	
	/**
	 * Display Patron
	 */
	public static void displayPatron() {
		for(Patron p : patrons) {
			System.out.println(p.toString());
		} if(patrons.isEmpty()) {
			System.out.println("There are no patrons in the system.");
		}
	}
	
	/**
	 * Edits Patron
	 */
	public static void editPatron() {
		Scanner input = new Scanner(System.in);
		System.out.println("Select who'd you like to edit");
		displayPatron();
		System.out.print("Patron ID: ");
		int patron = input.nextInt();
		if(patrons.get(patron) != null) {
			System.out.println("Editing Patron ID: #" + patrons.get(patron).getID());
			System.out.print("Enter the first name of the Patron: ");
			String firstName = input.next();
			patrons.get(patron).setFirstName(firstName);
			System.out.print("Enter the last name of the Patron: ");
			String lastName = input.next();
			patrons.get(patron).setLastName(lastName);
			System.out.println("Patron Updated.");
		} else {
			System.out.println("This Patron does not exist.");
		}
	}
	
	/**
	 * Displays the Main Menu
	 */
	public static void displayMenu() {
		StringBuilder menu = new StringBuilder();
		menu.append("===== Library Menu =====\n")
		.append("Please select an option:\n")
		.append("1| Add Patron\n")
		.append("2| Add Book\n")
		.append("3| Edit Patron\n")
		.append("4| Edit Book\n")
		.append("5| Display All Patrons\n")
		.append("6| Display All Books\n")
		.append("7| Check Out Book\n")
		.append("8| Check In Book\n")
		.append("9| Exit\n");
		System.out.println(menu.toString());
	}
}
