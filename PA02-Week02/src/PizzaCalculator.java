import java.util.Scanner;

public class PizzaCalculator {

	public static void main (String [] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter the cost of a whole pizza: $");
		double pizzaPrice = input.nextDouble();
		
		System.out.print("Enter the amount of slices: ");
		int pizzaSlice = input.nextInt();
		
		double pizzaPriceTotal = pizzaPrice / pizzaSlice;
		
		System.out.println("The cost for one slice of pizza is: $" + pizzaPriceTotal);

	}

}
