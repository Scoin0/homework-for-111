import java.util.Scanner;

public class CarStats {

	public static void main (String [] args) {
		
		String name, vehicleModel;
		double numOfGallons, kilometersTraveled, milesPerGallon, kilometersPerLiter, fromGallonsToLiters;
		int initialOdometer, finalOdometer, milesDriven;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter your name: ");
		name = input.next();
		System.out.println("Enter the cars model: ");
		vehicleModel = input.next();
		System.out.println("Enter the number of gallons used: ");
		numOfGallons = input.nextDouble();
		System.out.println("Enter the inital odometer reading: ");
		initialOdometer = input.nextInt();
		System.out.println("Enter the final odometer reading: ");
		finalOdometer = input.nextInt();
		
		milesDriven = finalOdometer - initialOdometer;
		kilometersTraveled = milesDriven * 1.609344;
		milesPerGallon = milesDriven / numOfGallons;
		fromGallonsToLiters = numOfGallons * 3.785412;
		kilometersPerLiter = kilometersTraveled / fromGallonsToLiters;
		
		System.out.println(name + ", here are your stats for your " + vehicleModel + ".");
		System.out.println("Your inital odometer reading was: " + initialOdometer);
		System.out.println("Your final odometer reading was: " + finalOdometer);
		System.out.println("You drove: " + milesDriven + " miles.");
		System.out.println("Kilometers traveled: " + kilometersTraveled);
		System.out.println("Miles per gallon: " + milesPerGallon);
		System.out.println("Kilometers per liter: " + kilometersPerLiter);

	}
}