
public class Zoo {
	
	public static void main(String[] args) throws InterruptedException {
		Panda panda = new Panda(true, false, false, "Forest", 4, "Black and White", "Bamboo");
		Hawk hawk = new Hawk(true, true, false, "Mountains", 2, true, "adult");
		Dog dog = new Dog(true, false, true, "Home", 4, 5, "brown");
		
		Panda panda1 = new Panda(true, false, false, "Plains", 4, "Gray and White", "Grass");
		Hawk hawk1 = new Hawk(true, true, false, "Hills", 2, false, "child");
		Dog dog1 = new Dog(true, false, true, "Town", 4, 8, "black");
		
		System.out.println("First Panda: ");
		System.out.println(panda.toString());
		panda.sneeze();
		System.out.println("----");
		System.out.println("Second Panda: ");
		System.out.println(panda1.toString());
		panda1.searchForFood("Food");
		System.out.println("----");
		System.out.println("First Hawk:");
		System.out.println(hawk.toString());
		hawk.sitOnEggs(5);
		System.out.println("----");
		System.out.println("Second Hawk:");
		System.out.println(hawk1.toString());
		hawk1.call();
		System.out.println("----");
		System.out.println("First Dog");
		System.out.println(dog.toString());
		dog.howl();
		System.out.println("----");
		System.out.println("Second Dog: ");
		System.out.println(dog1.toString());
		dog1.beg();
	}
}
