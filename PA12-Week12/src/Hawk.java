
/**
 * This class represents a Hawk.
 * 
 * @author John Lewis
 *
 */

public class Hawk extends Animal {
	
	private boolean hasNest;
	private String maturity;
	
	/**
	 * Creates a new Hawk.
	 */
	
	public Hawk(boolean canWalk, boolean canFly, boolean canSwim, String habitat, int numLegs, boolean hasNest, String maturity) {
		super(canWalk, canFly, canSwim, habitat, numLegs);
		this.hasNest = hasNest;
		this.maturity = maturity;
	}
	
	/**
	 * Returns if the hawk has a nest.
	 * 
	 * @return	boolean hasNest.
	 */
	
	
	public boolean getHasNest() {
		return hasNest;
	}
	
	/**
	 * Sets whether or not the Hawk has a nest.
	 * 
	 * @param hasNest		Whether or not the Hawk has a nest.
	 */

	public void setHasNest(boolean hasNest) {
		this.hasNest = hasNest;
	}
	
	/**
	 * Returns the stage of the hawk.
	 * 
	 * @return	String maturity.
	 */
	
	public String getMaturity() {
		return maturity;
	}
	
	/**
	 * Sets the stage of the Hawk
	 * 
	 * @param maturity		The stage of the Hawk
	 */
	
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
	
	/**
	 * calls
	 * 
	 */
	
	public void call() {
		System.out.println("Hawk: kee-eeeee-arr!");
	}
	
	/**
	 * Sits on eggs for a given time.
	 * 
	 * @param time	time to sit on eggs.
	 */
	
	public void sitOnEggs(int time) throws InterruptedException {
		System.out.println("Hawk has begun to sit on eggs...");
		Thread.sleep(1000*time);
		System.out.println("Hawk is happy her eggs are protected.");
	}
	
	/**
	 * Dipslay's the Hawk's information.
	 */
	
	@Override
	public String toString() {
		return	"This animal " +
				"is " + this.maturity + " and " +
				(this.hasNest ? "has a nest, " : "does not have a nest, ") +
				(this.canFly ? "can fly, " : "can't fly, ") +
				(this.canSwim ? "can swim, " : "can't swim, ") +
				(this.canWalk ? "can walk, " : "can't walk, ") +
				"lives in the " + this.habitat + ", and has " + this.numLegs + " leg(s)";
	}
}
