
/**
 * This class represents a Hawk.
 * 
 * @author John Lewis
 *
 */

public class Dog extends Animal {
	
	private int age;
	private String color;
	
	/**
	 * Creates a new Dog.
	 */
	
	public Dog(boolean canWalk, boolean canFly, boolean canSwim, String habitat, int numLegs, int age, String color) {
		super(canWalk, canFly, canSwim, habitat, numLegs);
		this.age = age;
		this.color = color;
	}
	
	/**
	 * Returns the age for the Dog.
	 * 
	 * @return	int age.
	 */
	
	public int getAge() {
		return age;
	}

	/**
	 * Sets the Dog's Age
	 * 
	 * @param age		The Dog's Age.
	 */

	public void setAge(int age) {
		this.age = age;
	}
	
	/**
	 * Returns the color for the Dog.
	 * 
	 * @return	String color.
	 */

	public String getColor() {
		return color;
	}

	/**
	 * Sets the Dog's color
	 * 
	 * @param color		The Dog's Color.
	 */

	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Howl
	 * 
	 */
	
	public void howl() {
		System.out.println("Dog howls in the night \n awwooo!");
	}
	
	/**
	 * Beg
	 * 
	 */
	
	public void beg() {
		System.out.println("Dog begs for some food!");
	}



	/**
	 * Dipslay's the Dog's information.
	 */
	
	@Override
	public String toString() {
		return	"This animal " +
				"color is " + this.color + " and " +
				"it's age is " + this.age +
				(this.canFly ? " can fly, " : " can't fly, ") +
				(this.canSwim ? "can swim, " : "can't swim, ") +
				(this.canWalk ? "can walk, " : "can't walk, ") +
				"lives in the " + this.habitat + ", and has " + this.numLegs + " leg(s)";
	}
}
