
/**
 * This class represents a Panda.
 * 
 * @author John Lewis
 *
 */

public class Panda extends Animal {
	
	private String food;
	private String color;
	
	/**
	 * Creates a new Panda.
	 */
	
	public Panda(boolean canWalk, boolean canFly, boolean canSwim, String habitat, int numLegs, String color, String food) {
		super(canWalk, canFly, canSwim, habitat, numLegs);
		this.color = color;
		this.food = food;
	}
	
	/**
	 * Returns the given food for the Panda.
	 * 
	 * @return	String food.
	 */
	
	public String getFood() {
		return food;
	}
	
	/**
	 * Sets the Panda's food
	 * 
	 * @param food		The Panda's food.
	 */
	
	public void setFood(String food) {
		this.food = food;
	}
	
	/**
	 * Returns the given colors for the Panda.
	 * 
	 * @return	String color.
	 */
	
	public String getColor() {
		return color;
	}
	
	/**
	 * Sets the Panda's color
	 * 
	 * @param color		The Panda's color.
	 */
	
	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Sneezes
	 * 
	 */
	
	public void sneeze() {
		System.out.println("Aaah--CHUU!!!...You've scared the mama panda");
	}
	
	/**
	 * Hunts for a given food.
	 * 
	 * @param food	searches for food.
	 */
	
	public void searchForFood(String food) throws InterruptedException {
		System.out.println("Panda is on the hunt...");
		Thread.sleep(2000);
		System.out.println("Panda has found " + food + "!");
	}
	
	/**
	 * Dipslay's the Panda's information.
	 */
	
	@Override
	public String toString() {
		return 	"This animal " +
				"has the colors of " + this.color +
				" and enjoys eating " + this.food + " he " +
				(this.canFly ? "can fly, " : "can't fly, ") +
				(this.canSwim ? "can swim, " : "can't swim, ") +
				(this.canWalk ? "can walk, " : "can't walk, ") +
				"lives in the " + this.habitat + ", and has " + this.numLegs + " leg(s)";
		
	}
}
