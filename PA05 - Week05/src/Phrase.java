import java.util.Scanner;

public class Phrase {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//Initialize Variables
		String sentence, word;
		
		//Console Output
		System.out.println("| This program will take a given phrase and word then deconstruct it and add the word to the end of the phrase.");
		System.out.println();
		
		//Getting Input
		System.out.print("Please enter a sentence: ");
		sentence = input.nextLine();
		System.out.print("Please enter a word: ");
		word = input.nextLine();
		
		//Figuring out everything.
		System.out.println();
		System.out.println("Here's your string in lower case: " + sentence.toLowerCase());
		System.out.println("Here's your string in uppper case: " + sentence.toUpperCase());
		System.out.println("The length of the sentence was: " + sentence.length());
		System.out.println("Is your word and sentence the same? " + sentence.equals(word));
		System.out.println("Here's the middle letter of your sentence: " + sentence.charAt(sentence.length()/2));
		System.out.println("Does your sentence contain your word? " + sentence.contains(word));
		System.out.println("Does your sentence contain the letter 'a'? " + sentence.contains("a"));
		System.out.println("Here's the sentence and word together: " + sentence.concat(word));
	}
}
