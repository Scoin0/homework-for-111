import java.text.DecimalFormat;
import java.util.Scanner;

public class CalculateBodyTemperature {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//Initialize variables. 
		int bodyTemp, surroundingTemp; 
		double time = 0;
		
		//Console Output
		System.out.println("| This program will calculate the body temperature of a person found in a basement.");
		System.out.println();
		System.out.print("Enter the temperature of the body upon discovery: ");
		bodyTemp = input.nextInt();
		System.out.print("Enter the surrounding temperature: ");
		surroundingTemp = input.nextInt();
		
		//Calculation
		time = Math.log((bodyTemp - surroundingTemp) / (98.6 - surroundingTemp)) / -0.1947;
		DecimalFormat decimalFormat = new DecimalFormat("####.##");
		System.out.print("The person has been dead for " + decimalFormat.format(time) + " hours.");	
		
	}
}
