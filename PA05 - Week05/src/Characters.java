import java.util.Scanner;

public class Characters {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//Initialize Variables
		String chara;
		
		/*UNUSED
		 *String vowels = "AEIOUaeiou";
		  String consonants = "BCDFGHJKLMNPQRSTVXWYZ";
		  String odd = "13579";
		  String even = "02468";
		 */
		
		//Console Output
		System.out.println("| This program will take a single character and figure out if it's a UPPERCASE, lowercase, or an even or odd number.");
		System.out.println();
		System.out.print("Enter a single chracter: ");
		chara = input.nextLine();
		char c = chara.charAt(0);
		
		//Figuring out if vowel, consonant, odd, or even
		
		//Uppercase + Vowel and Consonant
		if(Character.isUpperCase(c)){
			if(chara.contains("A") || chara.contains("E") || chara.contains("I") || chara.contains("O") || 
					chara.contains("U")) {
				System.out.print(c + " is a UPPERCASE letter and is a vowel");
			} else if (chara.contains("B") || chara.contains("C") || chara.contains("D") || chara.contains("F") || 
					chara.contains("G") || chara.contains("H") || chara.contains("J") || chara.contains("K") || 
					chara.contains("L") || chara.contains("M") || chara.contains("N") || chara.contains("P") || 
					chara.contains("Q") || chara.contains("R") || chara.contains("S") || chara.contains("T") || 
					chara.contains("V") || chara.contains("X") || chara.contains("W") || chara.contains("Y") || 
					chara.contains("Z")) {
				System.out.print(c + " is a UPPERCASE letter and is a consonant");
			}
		//Lowercase + Vowel and Consonant
		} else if (Character.isLowerCase(c)) {
			if(chara.contains("a") || chara.contains("e") || chara.contains("i") || chara.contains("o") || 
					chara.contains("u")) {
				System.out.println(c + " is a lowercase letter and is a vowel");
			} else if (chara.contains("b") || chara.contains("c") || chara.contains("d") || chara.contains("f") || 
					chara.contains("g") || chara.contains("h") || chara.contains("j") || chara.contains("k") || 
					chara.contains("l") || chara.contains("m") || chara.contains("n") || chara.contains("p") || 
					chara.contains("q") || chara.contains("r") || chara.contains("s") || chara.contains("t") || 
					chara.contains("v") || chara.contains("x") || chara.contains("w") || chara.contains("y") || 
					chara.contains("z")) {
				System.out.println(c + " is a lowercase letter and is a consonant");
			}
		//Number + Odd and Even
		} else if (Character.isDigit(c)) {
			if(chara.contains("1") || chara.contains("3") || chara.contains("5") || chara.contains("7") || 
					chara.contains("9")) {
				System.out.println(c + " is an odd number");
			} else if (chara.contains("2") || chara.contains("4") || chara.contains("6") || chara.contains("8") ||
					chara.contains("0")) {
				System.out.println(c + " is an even number");
			}
			
		} else {
			System.out.println(c + " is something other than a letter or a number");
		}
		
		/**
		 * Even I'm confused at this big clunky bulk of text. I know this could be done more easily but I can't think
		 * of that solution right now. I had thought to make an character array and trying to read from that but wasn't
		 * sure if that was the right solution. So instead this was the solution I went with. 
		 */
		
		
	}
}
