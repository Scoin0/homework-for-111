import java.util.Scanner;

public class Airplane {
	
	//Initialize Variables
	static Scanner input = new Scanner(System.in);
	static int choice, row, seat;
	static char[][] seating = {
			{'A', 'B', 'C'},
			{'A', 'B', 'C'},
			{'A', 'B', 'C'},
	};
	
	//Sets the array value
	public static void seatingReservation() {
		System.out.println();
		System.out.println("Seating Reservation: ");
		System.out.println();
		System.out.print("Select Row (1, 2 ,3): ");
		row = input.nextInt();
		System.out.print("Select Seat (1=A, 2=B, 3=C): ");
		seat = input.nextInt();
		
		if(row >= 4 || row <= 0 || seat <= 0 || seat >= 4) {
			System.out.println("Invalid Choice.");
			System.out.println();
			System.out.println("System Menu:");
			System.out.println("| 1: Select Seat");
			System.out.println("| 2: Print Seating Chart");
			System.out.println("| 3: Exit");
			System.out.print("Choice: ");
			choice = input.nextInt();
		} else {
			System.out.println();
			seating[row-1][seat-1] = 'x';
			System.out.println("System Menu:");
			System.out.println("| 1: Select Seat");
			System.out.println("| 2: Print Seating Chart");
			System.out.println("| 3: Exit");
			System.out.print("Choice: ");
			choice = input.nextInt();
		}
	}
	
	//Loops through the array and prints out the values for the seats.
	public static void printChart() {
		int temp = 0;
		System.out.println("Seating Arragement:");
		for(int i = 0; i < seating.length; i++) {
			System.out.print("Row " + (temp = temp + 1) + ": ");
			System.out.println(seating[i]);
		}
	}
	
	public static void main(String[] args) {
		
		//Console Output
		System.out.println("| This program will display seating arragements in a small plane.");
		System.out.println("| By picking a seat, you reserve the seat.");
		System.out.println();
		System.out.println("System Menu:");
		System.out.println("| 1: Select Seat");
		System.out.println("| 2: Print Seating Chart");
		System.out.println("| 3: Exit");
		System.out.print("Choice: ");
		choice = input.nextInt();
		
		//Calculation
		while(choice <= 3) {
			if(choice == 1) {
				seatingReservation();
			}
			
			if(choice == 2) {
				System.out.println();
				printChart();
				System.out.println();
				System.out.println("System Menu:");
				System.out.println("| 1: Select Seat");
				System.out.println("| 2: Print Seating Chart");
				System.out.println("| 3: Exit");
				System.out.print("Choice: ");
				choice = input.nextInt();
			}
			
			if(choice == 3) {
				System.exit(0);
			}
			
			if(choice <= 0) {
				System.out.println("Invaild Choice, Please try again.");
				System.out.println();
				System.out.print("Choice: ");
				choice = input.nextInt();
			}
		}
		
		while(choice >= 4) {
			System.out.println("Invaild Choice, Please try again.");
			System.out.println();
			System.out.print("Choice: ");
			choice = input.nextInt();
		}
	}
}
