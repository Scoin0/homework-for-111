
public class Rectangle extends GeometricObject implements Comparable{
	
	private double width;
	private double height;
	
	/*
	 * This creates a default rectangle
	 */
	public Rectangle() {
		this(1.0, 1.0);
	}
	
	/**
	 * @param width 	Gets the width of the rectangle
	 * @param height 	Gets the height of the rectangle
	 */
	public Rectangle(double width, double height) {
		this.width = width;
		this.height = height;
	}

	/**
	 * @return the width
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(double height) {
		this.height = height;
	}
	
	/**
	 * Get Area
	 */
	public double getArea() {
		return width * height;
	}
	
	/**
	 * Get Perimeter
	 */
	public double getPerimeter() {
		return 2 * (width + height);
	}
	
	/**
	 * @param o  Object to compare to
	 */
	public int compareTo(Object o) {
		if(this.getArea() > ((Rectangle) o).getArea()) {
			return 1;
		}else if(this.getArea() < ((Rectangle) o).getArea()) {
			return -1;
		} else {
			return 0;
		}
	}
	
	/**
	 * @param o  Object to equal to
	 */
	public boolean equals(Object o) {
		return this.getArea() == ((Rectangle) o).getArea();
	}
}
