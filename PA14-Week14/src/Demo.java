
public class Demo {
	
	public static void main(String[] args) {
		
		System.out.println("| This program take two shapes and compare their area.");
		System.out.println("| It will also give details about themselves\n");
		Rectangle();
		System.out.println();
		Triangle();
	}
	
	public static void Rectangle() { 
		System.out.println("-----Rectangles-----");
		Rectangle rect = new Rectangle();
		Rectangle rect1 = new Rectangle(12, 55);
		System.out.println("First some information about the given rectangles: ");
		System.out.println("Rectangle 1: ");
		System.out.println("The Rectangle's width is: " + rect.getWidth());
		System.out.println("The Rectangle's height is: " + rect.getHeight());
		System.out.println("The Rectangle's area is: " + rect.getArea());
		System.out.println("The Rectangle's perimeter is: " + rect.getPerimeter() + "\n");
		System.out.println("Rectangle 2: ");
		System.out.println("The Rectangle's width is: " + rect1.getWidth());
		System.out.println("The Rectangle's height is: " + rect1.getHeight());
		System.out.println("The Rectangle's area is: " + rect1.getArea());
		System.out.println("The Rectangle's perimeter is: " + rect1.getPerimeter() + "\n");
		System.out.println("Are the two rectangles equal to each other? " + rect.equals(rect1));
		System.out.println("How do they compare to each other? " + rect.compareTo(rect1));
		System.out.println("1 = Bigger, -1 = Smaller");
		System.out.println("-----END-----");
	}
	
	public static void Triangle() {
		System.out.println("-----Triangles-----");
		Triangle tri = new Triangle(5, 14);
		Triangle tri1 = new Triangle(15, 22);
		Triangle tri2 = new Triangle(5, 14);
		System.out.println("First some information about the given triangles: ");
		System.out.println("Triangle 1: ");
		System.out.println("The Triangle's width is: " + tri.getWidth());
		System.out.println("The Triangle's height is: " + tri.getHeight());
		System.out.println("The Triangle's area is: " + tri.getArea());
		System.out.println("The Triangle's perimeter is: " + tri.getPerimeter() + "\n");
		System.out.println("Triangle 2: ");
		System.out.println("The Triangle's width is: " + tri1.getWidth());
		System.out.println("The Triangle's height is: " + tri1.getHeight());
		System.out.println("The Triangle's area is: " + tri1.getArea());
		System.out.println("The Triangle's perimeter is: " + tri1.getPerimeter() + "\n");
		System.out.println("Triangle 3: ");
		System.out.println("The Triangle's width is: " + tri2.getWidth());
		System.out.println("The Triangle's height is: " + tri2.getHeight());
		System.out.println("The Triangle's area is: " + tri2.getArea());
		System.out.println("The Triangle's perimeter is: " + tri2.getPerimeter() + "\n");
		System.out.println("Are the three rectangles equal to each other? \n" + "Tri1 equals Tri2: " + tri.equals(tri1) + ", Tri2  equals Tri3: " + tri1.equals(tri2) +  ", Tri3  equals Tri1: " +tri2.equals(tri));
		System.out.println("How do they compare to each other? \n" + "Tri1 compared to Tri2: " + tri.compareTo(tri1) + ", Tri2 compared to Tri3: " + tri1.compareTo(tri2) + ", Tri3 compared to Tri1: " + tri2.compareTo(tri));
		System.out.println("1 = Bigger, -1 = Smaller, 0 = Same");
		System.out.println("-----END-----");
	}

}
