
public class Triangle extends GeometricObject implements Comparable{
	
	private double width;
	private double height;
	public double hypo = Math.pow(Math.pow(width, 2) + Math.pow(height, 2), 2);
	
	/*
	 * This creates a default triangle
	 */
	public Triangle() {
		this(1.0, 1.0);
	}
	
	/**
	 * @param width 	Gets the width of the triangle
	 * @param height 	Gets the height of the triangle
	 */
	public Triangle(double width, double height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * @return the width
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(double height) {
		this.height = height;
	}
	
	/**
	 * @param o  Object to compare to
	 */
	public int compareTo(Object o) {
		if(this.getArea() > ((Triangle)o).getArea()) {
			return 1;
		} else if(this.getArea() < ((Triangle)o).getArea()) {
			return -1;
		} else {
			return 0;
		}
	}
	
	/**
	 * @param o  Object to equal to
	 */
	public boolean equals(Object o) {
		return this.getArea() == ((Triangle)o).getArea();
	}
	
	/**
	 * Get Area
	 */
	public double getArea() {
		return (width * height) / 2;
	}
	
	/**
	 * Get Perimeter
	 */
	public double getPerimeter() {
		return width + height + hypo;
	}
}
