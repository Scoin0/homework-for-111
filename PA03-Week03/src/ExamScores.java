import java.util.Scanner;

public class ExamScores {
	
	static double exam1, exam2, exam3, examTotal, examSum;
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//Getting the 3 exam scores
		System.out.print("Enter the first exam score: ");
		exam1 = input.nextDouble();
		System.out.print("Enter the second exam score: ");
		exam2 = input.nextDouble();
		System.out.print("Enter the third exam score: ");
		exam3 = input.nextDouble();
		System.out.println();
		System.out.println();
		
		//Calculating Exam Scores and printing score
		examSum = ((exam1 + exam2 + exam3));
		examTotal = ((exam1 + exam2 + exam3) / 3);
		System.out.println("Exam Total: " + examSum);
		
		//Figuring out what grade to give
		if(examTotal >= 90) {
			System.out.println("Final Grade: A");
		}else if(examTotal >= 80 && examTotal <= 89) {
			System.out.println("Final Grade: B");
		}else if(examTotal >= 70 && examTotal <= 79) {
			System.out.println("Final Grade: C");
		}else if(examTotal >= 60 && examTotal <= 69) {
			System.out.println("Final Grade: D");
		}else {
			System.out.println("Final Grade: F");
		}
	}
}
