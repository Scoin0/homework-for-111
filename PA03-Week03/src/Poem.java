import java.util.Random;

public class Poem {
	
	static Random random = new Random();
	
	public static void main(String[] args) {
		
		//Getting a random number between 1-9
		int poemRandom = random.nextInt(9) + 1;
		
		//Getting the right poem for the number generated
		switch(poemRandom) {
		
		default:
			System.out.println("Sorry, invalid number. (" + poemRandom + ")");
			break;
		case 1:
			System.out.print("Monday's child is fair of face");
			break;
		case 2:
			System.out.print("Tuesday's child is full of grace");
			break;
		case 3:
			System.out.print("Wednesday's child is full of woe");
			break;
		case 4:
			System.out.print("Thursday's child has far to go");
			break;
		case 5:
			System.out.print("Friday's child is loving and giving");
			break;
		case 6:
			System.out.print("Saturday's child works hard for its living");
			break;
		case 7:
			System.out.print("But the child that is born on the Sabbath day is bonny and blithe, and good and gay");
			break;
		}
	}
}
