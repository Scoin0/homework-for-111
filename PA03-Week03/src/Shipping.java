import java.text.DecimalFormat;
import java.util.Scanner;

public class Shipping {
	
	static double subtotalAmount, taxRate, totalSpending, shippingCost, tax;
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//Getting the subtotal amount and taxrate
		System.out.print("Enter the subtotal amount: $" );
		subtotalAmount = input.nextDouble();
		System.out.print("Enter the tax rate as a percentage (example: 4.5): ");
		taxRate = input.nextDouble();
		
		//Getting the shipping amount
		if(subtotalAmount <= 25) {
			shippingCost = 5;
		}else if(subtotalAmount >= 26 && subtotalAmount <= 50) {
			shippingCost = 8;
		}else if(subtotalAmount >= 51 && subtotalAmount <= 75) { 
			shippingCost = 12;
		}else if(subtotalAmount >= 76 && subtotalAmount <= 100) {
			shippingCost = 18;
		}else {
			shippingCost = 0;
		}
		
		//Calculation
		tax = (subtotalAmount * (taxRate / 100.00));
		DecimalFormat decimalformat = new DecimalFormat("##.##");
		totalSpending = (subtotalAmount + tax + shippingCost);
		
		//Final Output
		System.out.println("Invoice: ");
		System.out.println("Subtotal: $" + subtotalAmount);
		System.out.println("Tax: $" + decimalformat.format(tax));
		System.out.println("Shipping: $" + shippingCost);
		System.out.println("Total: $" + decimalformat.format(totalSpending));
		
	}
}
