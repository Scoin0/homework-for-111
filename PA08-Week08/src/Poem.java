import java.util.Scanner;

public class Poem {
	
	//Initialize Variable
	static int inputNumber;
	
	//Depending on the number typed, print that line
	public static void printLine(int number) {
		number = inputNumber;
		
		if(inputNumber == 0) {
			System.exit(0);
		}
		
		if(inputNumber == 1) {
			System.out.println("Monday's child is fair of face");
		}
		
		if(inputNumber == 2) {
			System.out.println("Tuesday's child is full of grace");
		}
		
		if(inputNumber == 3) {
			System.out.println("Wednesday's child is full of woe");
		}
		
		if(inputNumber == 4) {
			System.out.println("Thursday's child has far to go");
		}
		
		if(inputNumber == 5) {
			System.out.println("Friday's child is loving and giving");
		}
		
		if(inputNumber == 6) {
			System.out.println("Saturday's child works hard for its living");
		}
		
		if(inputNumber == 7) {
			System.out.println("But the child that is born on the Sabbath day is bonny and blithe, and good and gay");
		}	
	}
	
	public static void main(String[] args) {
		
		//Initialize Variables
		Scanner input = new Scanner(System.in);
		
		//Console Output
		System.out.println("| This program will display a poem depending on the number input.");
		System.out.println("| It will do this by sending the input number to a method and having the method display the poem.");
		System.out.println();
		System.out.print("Enter a number between 1 and 7 (0 to exit): ");
		inputNumber = input.nextInt();
		
		//Calculation
		while(inputNumber <= 7) {
			printLine(inputNumber);
			System.out.println();
			System.out.print("Enter a number between 1 and 7 (0 to exit): ");
			inputNumber = input.nextInt();
		}
		
		while(inputNumber >= 8) {
			System.out.println("The number you have entered was not between 1-7.");
			System.out.println();
			System.out.print("Enter a number between 1 and 7 (0 to exit): ");
			inputNumber = input.nextInt();
		}
	}
}

		
