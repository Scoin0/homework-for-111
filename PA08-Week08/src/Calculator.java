import java.util.Scanner;

public class Calculator {
	
	//Addition
	public static void add(double num1, double num2) {
		double answer;
		answer = num1 + num2;
		System.out.println(num1 + " + " + num2 + " = " + answer);
	}
	
	//Subtraction
	public static void subtract(double num1, double num2) {
		double answer;
		answer = num1 - num2;
		System.out.println(num1 + " - " + num2 + " = " + answer);
	}
	
	//Multiplication
	public static double multiply(double num1, double num2) {
		return num1 * num2;
	}
	
	//Division
	public static double division(double num1, double num2) {
		return num1 / num2;
	}
	
	public static void main(String[] args) {
		
		//Initialize Variables
		Scanner input = new Scanner(System.in);
		double number1, number2;
		int option;
		
		//Console Output
		System.out.println("| Simple Calculator");
		System.out.println();
		System.out.println("Select an Option: ");
		System.out.println("1| Addition");
		System.out.println("2| Subtraction");
		System.out.println("3| Multiplication");
		System.out.println("4| Division");
		System.out.print("Option: ");
		option = input.nextInt();
		
		while(option <= 4) {
			if(option == 1) {
				System.out.print("Enter the First Number: ");
				number1 = input.nextDouble();
				System.out.print("Enther the Second Number:  ");
				number2 = input.nextDouble();
				add(number1, number2);
			}
			
			if(option == 2) {
				System.out.print("Enter the First Number: ");
				number1 = input.nextDouble();
				System.out.print("Enther the Second Number:  ");
				number2 = input.nextDouble();
				subtract(number1, number2);
			}
			
			if(option == 3) {
				System.out.print("Enter the First Number: ");
				number1 = input.nextDouble();
				System.out.print("Enther the Second Number:  ");
				number2 = input.nextDouble();
				System.out.println(number1 + " * " + number2 + " = " + multiply(number1, number2));
			}
			
			if(option == 4) {
				System.out.print("Enter the First Number: ");
				number1 = input.nextDouble();
				System.out.print("Enther the Second Number:  ");
				number2 = input.nextDouble();
				System.out.println(number1 + " / " + number2 + " = " + division(number1, number2));
			}
			
			System.out.println("--------------------------");
			System.out.println();
			System.out.println("Select an Option: ");
			System.out.println("1| Addition");
			System.out.println("2| Subtraction");
			System.out.println("3| Multiplication");
			System.out.println("4| Division");
			System.out.print("Option: ");
			option = input.nextInt();
		}
		
		while(option >= 5 || option < 1) {
			System.out.println();
			System.out.println("You've selected a number that was not part of the choices.");
			System.out.println("Select an Option: ");
			System.out.println("1| Addition");
			System.out.println("2| Subtraction");
			System.out.println("3| Multiplication");
			System.out.println("4| Division");
			System.out.print("Option: ");
			option = input.nextInt();
		}
	}
}
