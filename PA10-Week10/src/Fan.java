
public class Fan {
	
	public static void main(String[] args) {
		
		System.out.println("| This program will display fan settings");
		System.out.println("| Each object this Fan class is creating is a new Fan1 object\n");
		
		Fan1 fan1 = new Fan1();
		fan1.setSpeed(fan1.fast);
		fan1.setRadius(10);
		fan1.setColor("black");
		fan1.setOn(true);
		System.out.println(fan1.toString());
		
		Fan1 fan2 = new Fan1();
		fan2.setSpeed(fan2.medium);
		fan2.setRadius(5);
		fan2.setColor("purple");
		fan2.setOn(false);
		System.out.println(fan2.toString());
		
	}

}

class Fan1 {
	
	public static int slow = 1;
	public static int medium = 2;
	public static int fast = 3;
	
	private int speed = slow;
	private boolean isOn = false;
	private double radius = 5;
	private String color = "white";
	
	public Fan1() {
		
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public String toString() {
		return  "Fan Status:\n"
				+ "Speed: " + speed + "\n"
				+ "Color: " + color + "\n" 
				+ "Radius: " + radius + "\n"
				+ ((isOn) ? "Fan is on\n" : "Fan is off\n");
	}
	
}