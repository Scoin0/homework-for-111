import java.text.DecimalFormat;
import java.util.Scanner;

public class CreateCar {
	
	static String tempMake;
	static String tempModel;
	static String tempColor;
	static int tempYear;
	static double tempCost;
	
	public static void main(String[] args) {
		
		//Initialize Variables
		Scanner input = new Scanner(System.in);
		DecimalFormat format = new DecimalFormat("###,###,###.##");
		
		System.out.println("| Below we'll be making 2 cars, one is a default car and the other will accept values given");
		System.out.println("| Honesty, I thought it would be easier to just seperate the class like this... So that's what I did.\n");
		
		//Create the Car 1
		Car dCar = new Car();
		dCar.setMake("Toyota");
		dCar.setModel("Highlander");
		dCar.setCost(31695);
		System.out.println("Here's Car 1: \n" + dCar.toString());
		
		//Getting Custom Values for new Car
		Car car = new Car();
		
		System.out.print("What's the make of the car: ");
		tempMake = input.nextLine();
		car.setMake(tempMake);
		
		System.out.print("What's the model of the car: ");
		tempModel = input.nextLine();
		car.setModel(tempModel);
		
		System.out.print("What year is the car: ");
		tempYear = input.nextInt();
		car.setYear(tempYear);
		
		System.out.print("How much does this car cost: ");
		tempCost = input.nextDouble();
		car.setCost(tempCost);
		
		System.out.print("What color is the car: ");
		tempColor = input.next();
		car.setColor(tempColor);
		
		System.out.println();
		System.out.println(car.toString());
	}
}
