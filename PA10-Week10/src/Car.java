import java.text.DecimalFormat;

public class Car {
	
	//Setting Variables
	private String make = "Make";
	private String model = "Model";
	private String color = "White";
	private int year = 2018;
	private double cost = 30000;
	
	//no-args Constructor
	public Car() {

	}
	
	//All Getters and Setters
	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}


	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public double getCost() {
		return cost;
	}
	
	//toString method for displaying the Car
	public String toString() {
		DecimalFormat format = new DecimalFormat("###,###,###.##");
		return "The created car:\n"
				+ "Make: " + make + "\n"
				+ "Model: " + model + "\n"
				+ "Year: " + year + "\n"
				+ "Cost: $" + format.format(cost) + "\n"
				+ "Color: " + color + "\n";
	}
}
