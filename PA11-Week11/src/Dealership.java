import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Dealership {
	
	static Car[] car = new Car[11];
	static Scanner input = new Scanner(System.in);
	static Scanner carInput = new Scanner(System.in);
	static DecimalFormat format = new DecimalFormat("###,###,###.##");
	static int ID, carMenu;
	static String tempMake;
	static String tempModel;
	static String tempColor;
	static int tempYear;
	static double tempCost;
	
	//I know I'm creating over 10 cars. I'm doing so that the number starts at 1 since the while loop is looking
	//for something that's not 0
	public static void createCars() {
		for(int i = 0; i < 11; i++) {
			car[i] = new Car();
		}
	}
	
	public static void main(String[] args) {
		
		System.out.println("| This program will simulate an inventory system for a dealship.");
		System.out.println("| You can manage the inventory of Cars here.\n");
		
		createCars();
		
		System.out.print("Enter the ID of the car you want to change: ");
		ID = input.nextInt();
		
		
		ID:
		while(ID != 0) {

			if(ID < 11) {
				System.out.println("Confirmed Entry of Car ID: " + ID);
				
				System.out.println();
				
				System.out.println("Car Information Menu: ");
				System.out.println("1| Set Make");
				System.out.println("2| Set Model");
				System.out.println("3| Set Year");
				System.out.println("4| Set Cost");
				System.out.println("5| Set Color");
				System.out.println("6| Display Car Information");
				System.out.println("7| Exit and Choose New Car ID");
				System.out.print("Input: ");
				
				carMenu = input.nextInt();
				
				System.out.println("");
				
				CarMenu:
				while(carMenu != 0) {
					
					if(carMenu == 1) {
						System.out.print("What's the make of the car: ");
						tempMake = carInput.nextLine();
						car[ID].setMake(tempMake);
						
						System.out.print("Input: ");
						carMenu = input.nextInt();
						
					} else if(carMenu == 2) {
						System.out.print("What's the model of the car: ");
						tempModel = carInput.nextLine();
						car[ID].setModel(tempModel);
						
						System.out.print("Input: ");
						carMenu = input.nextInt();
						
					} else if(carMenu == 3) {
						System.out.print("What year is the car: ");
						tempYear = carInput.nextInt();
						car[3].setYear(tempYear);
						
						System.out.print("Input: ");
						carMenu = input.nextInt();
						
					}else if(carMenu == 4) {
						System.out.print("How much does this car cost: ");
						tempCost = carInput.nextDouble();
						car[ID].setCost(tempCost);
						
						System.out.print("Input: ");
						carMenu = input.nextInt();
						
					}else if(carMenu == 5) {
						System.out.print("What color is the car: ");
						tempColor = carInput.next();
						car[ID].setColor(tempColor);
						
						System.out.print("Input: ");
						carMenu = input.nextInt();
						
					}else if(carMenu == 6) {
						System.out.println("Car Information for Car ID:" + ID);
						System.out.println(car[ID].toString());
						
						System.out.print("Input: ");
						carMenu = input.nextInt();
					}else if(carMenu == 7) {
						System.out.print("Enter the ID of the car you want to change: ");
						ID = input.nextInt();
						break CarMenu;
					}
				}
			}
			
			if(ID > 10) {
				System.out.println("Sorry, the number you entered is not on the list of IDs.");
				System.out.print("Enter the ID of the car you want to change: ");
				ID = input.nextInt();
			}
		}
	}
}
