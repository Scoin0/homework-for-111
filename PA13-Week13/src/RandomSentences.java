import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

public class RandomSentences {
	
	static String[] verbs = {
			"running",
			"walking",
			"dancing",
			"skipping",
			"prancing",};
	static String[] nouns = {
			"horse",
			"elephant",
			"ghost",
			"bird",
			"monkey",
			"cow",
			"hedgehog",
			"alpaca",
			"drone",
			"alligator",
			"dog",
			"elk",
			"fish"};
	static String[] colors = {
			"brown",
			"black",
			"purple",
			"red",
			"blue"};
	static String[] places = {
			"pub",
			"store",
			"bank",
			"gas station",
			"restaurant"};
	static Random random = new Random();
	
	
	//A {color} {noun} was {verb} down the street when it encountered a {noun} near the {place}
	
	private static int random(int length) {
		return random.nextInt(length);
	}
	
	
	public static void main(String[] args) {
		
		StringBuilder sB = new StringBuilder();
		File file = new File("RandomStrings.txt");

		System.out.println("Writing to file...");
		
		if(file.exists()) {
			System.out.println(file + " already exists.");
			System.exit(0);
		}
		
		try {
			PrintWriter op = new PrintWriter(file);
			for(int i = 0; i < 5; i++) {
				op.println("A " + colors[random(colors.length)] + " " 
			+ nouns[random(nouns.length)] + " was " + verbs[random(verbs.length)] 
			+ " down the street when it encountered a " + nouns[random(nouns.length)] 
			+ " near the " + places[random(places.length)] + ".");
			} 
			op.close();
		} catch (FileNotFoundException e) {
			System.out.println(file + " not found...");
			e.printStackTrace();
		}
		
		System.out.println("Finished Writing to file!\n");
		System.out.println("Here's what sentences it generated: ");
		
		try {
			Scanner input = new Scanner(file);
			while(input.hasNextLine()) {
				System.out.println(input.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println(file + " not found...");
			e.printStackTrace();
		}
	}
}
