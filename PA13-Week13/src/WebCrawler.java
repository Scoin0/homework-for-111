import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class WebCrawler {
	
	public static String Url = "https://windward.hawaii.edu/About_WCC/";
	
	public static void main(String[] args) {
		try {
			
			URL url = new URL(Url);
			Scanner input = new Scanner(url.openStream());
			int count = 0;
			
			while(input.hasNext()) {
				String line = input.next();
				
				if(line.equalsIgnoreCase("Windward")) {
					count++;
				}
			}
			
			System.out.println("I counted " + count + " times the webpage had the word 'Windward'.");
			
		} catch(MalformedURLException e) {
			System.out.println("Invalid URL");
			e.printStackTrace();
		} catch(IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		}
	}

}
